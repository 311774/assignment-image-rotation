#include "file_io.h"

enum open_status open_file(FILE** file, char* file_name, char* mode){
	*file = fopen(file_name, mode);
	if (file) {
		return OPEN_OK;
	} else {
		return OPEN_ERROR;
	}
}

enum close_status close_file(FILE** file){
	if (fclose(*file)) {
		return CLOSE_OK;
	} else {
		return CLOSE_ERROR;
	}
}
