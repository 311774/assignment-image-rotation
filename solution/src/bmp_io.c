#include "bmp_struct.h"
#include "bmp_io.h"
#include "image_struct.h"

static const uint16_t bfType = 19778;
static const uint32_t bOffBits = 54;
static const uint32_t biSize = 40;
static const uint32_t bfReserved = 0;
static const uint16_t biPlanes = 1;
static const uint16_t biBitCount = 24;
static const uint32_t biCompression = 0;
static const uint32_t biXPelsPerMeter = 2834;
static const uint32_t biYPelsPerMeter = 2834;
static const uint32_t biClrUsed = 0;
static const uint32_t biClrImportant = 0;


static size_t calculate_padding(size_t width){
	size_t padding = 4 - width * sizeof(struct pixel) % 4;
	if (padding == 4){
		padding = 0;
	}
	return padding;
}

static enum read_status read_pixels(FILE* file_in, struct image* img) {
    const uint64_t width = img->width;
    const uint64_t height = img->height;
    const size_t padding = calculate_padding(width);
	
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    if (data == NULL)
    	return READ_ERROR;
    struct pixel* read_data = data;
    for (size_t i = 0; i < height; i++) {
        const size_t result = fread(read_data, sizeof(struct pixel), width, file_in);
        if (result != width){
            free(read_data);
            return READ_ERROR;
        }
        if (fseek(file_in, padding, SEEK_CUR) != 0) {
            free(read_data);
            return READ_ERROR;
        }
        read_data = width + read_data;
    }
    
    img->data = data;
    return READ_OK;
}

static enum read_status read_bmp_header(FILE* file_in, struct bmp_header* header) {
    size_t header_count = fread(header, sizeof(struct bmp_header), 1, file_in);
    if (header_count != 1) {
	    return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static enum write_status write_header( FILE* file, uint64_t width, uint64_t height) {

	const size_t padding = calculate_padding(width);
    const uint64_t data_size = (width * 3 + padding) * height;
    struct bmp_header header = {
        .bfType = bfType,
        .bFileSize = bOffBits + data_size,
        .bfReserved = bfReserved,
        .bOffBits = bOffBits,
        .biSize = biSize,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = biPlanes,
        .biBitCount = biBitCount,
        .biCompression = biCompression,
        .biSizeImage = data_size,
        .biXPelsPerMeter = biXPelsPerMeter,
        .biYPelsPerMeter = biYPelsPerMeter,
        .biClrUsed = biClrUsed,
        .biClrImportant = biClrImportant
    };
	
	if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1) {
		return WRITE_ERROR;
	}
	return WRITE_OK;
}

enum read_status from_bmp(FILE *in, struct image *image) {
    struct bmp_header header;
    enum read_status status = read_bmp_header(in, &header);
    if (status) {
		return status;
    }
    image->width = header.biWidth;
    image->height = header.biHeight;
    return read_pixels(in, image);
}

enum write_status to_bmp(FILE *out, struct image *image) {
    if (out == NULL || image == NULL || image->data == NULL) {
    	return WRITE_ERROR;
    }
	
    const uint64_t width = image->width;
    const uint64_t height = image->height;
    const size_t padding = calculate_padding(width);
    
    enum write_status write_status = write_header(out, width, height);
    if (write_status) {
    	return write_status;
    }
    struct pixel* write_data = image->data;
    for (size_t i = 0; i < height; i++) {
        size_t pixels = fwrite(write_data, sizeof(struct pixel), width, out);
        if (pixels != width) {
		  return WRITE_ERROR;
	}
        size_t padding_count = fwrite(write_data, padding, 1, out);
        if (padding_count != 1) {
		  return WRITE_ERROR;
	}
        write_data = write_data + width;  
    }
    
    return WRITE_OK;
	
}

