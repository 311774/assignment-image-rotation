#include "image_struct.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height) {
	struct image new_image;
	new_image.width = width;
	new_image.height = height;
	new_image.data = NULL;
	return new_image;
}

struct image image_rotate(const struct image old_img) {
    struct image new_img = image_create(old_img.height, old_img.width);
    new_img.data = malloc(sizeof(struct pixel) * old_img.width * old_img.height);
    for (size_t i = 0; i < old_img.height; i++) {
        for (size_t j = 0; j < old_img.width; j++) {
			struct pixel old_pixel = old_img.data[i * old_img.width + j];
            new_img.data[j * old_img.height + old_img.height - i - 1] = old_pixel;
        }
    }
    return new_img;
}

void image_delete(struct image* image) {
	free(image->data);
}
