#include "bmp_io.h"
#include "file_io.h"
#include "image_rotate.h"
#include "image_struct.h"
#include <stdio.h>

int main(int argc, char** argv) {
    char* input_path = NULL;
    char* output_path = NULL;
    struct image img;
    FILE* input_file = NULL;
    FILE* output_file = NULL;
	
	if (argc == 3) {
		input_path = argv[1];
		output_path = argv[2];
				
		if (open_file(&input_file, input_path, "rb") == OPEN_OK) {
			fprintf(stderr, "Input file opened\n");
			if (open_file(&output_file, output_path, "wb") == OPEN_OK) {
				fprintf(stderr, "Output file opened\n");
				if (from_bmp(input_file, &img) == READ_OK) {
					fprintf(stdout, "BMP read successfully\n");
					struct image rotated = image_rotate(img);
					if (to_bmp(output_file, &rotated) == WRITE_OK) {
						fprintf(stdout, "Rotation successful\n");
						
					} else {
					
						fprintf(stderr, "Error, rotation not completed\n");
					}
					if (close_file(&input_file) == CLOSE_OK) {
						fprintf(stderr, "Input file closed\n");
						if (close_file(&output_file) == CLOSE_OK) {
							fprintf(stderr, "Output file closed\n");
							return 0;
						} else {
							fprintf(stderr, "Output file cannot be closed\n");
						}
					} else {
						fprintf(stderr, "Input file cannot be closed\n");
					}
					image_delete(&rotated);
					image_delete(&img);
				} else {
					fprintf(stderr, "BMP could not be read\n");
					image_delete(&img);
					close_file(&input_file);
					close_file(&output_file);
				}
			} else {
				fprintf(stderr, "Output file could not be opened\n");
				close_file(&input_file);
			}
		} else {
			fprintf(stderr, "Intput file could not be opened\n");
		} 
	} else {
		fprintf(stderr, "Wrong arguments!\n");
	}
	
	fprintf(stderr, "Success!\n");
	return 0;
}

