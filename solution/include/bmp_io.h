#ifndef BMP_IO_H
#define BMP_IO_H

#include <stdint.h>
#include <malloc.h>
#include <stdio.h>
#include "image_struct.h"

enum read_status {
	READ_OK = 0,
	READ_INVALID_HEADER,
	READ_ERROR
};

enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR
};

#endif

enum read_status from_bmp(FILE *in, struct image *image);

enum write_status to_bmp(FILE *out, struct image *image);

