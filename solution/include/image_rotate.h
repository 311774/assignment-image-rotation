#ifndef IMAGE_ROTATE
#define IMAGE_ROTATE

#include "image_struct.h"

struct image image_rotate(const struct image old_img);

void image_delete(struct image* img);

#endif

